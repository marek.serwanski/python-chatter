#!/usr/bin/env python3
import os
import time
import subprocess

from flask import Flask, request, jsonify
from flask_cors import CORS
from langchain.callbacks.base import BaseCallbackHandler
from langchain.chains import RetrievalQA
from langchain.embeddings import HuggingFaceEmbeddings
from langchain.llms import Ollama
from langchain.vectorstores import Chroma
from confluent_kafka import Producer
from datetime import datetime
import socket

# KAFKA CONF
kafka_conf = {'bootstrap.servers': 'pkc-7xoy1.eu-central-1.aws.confluent.cloud:9092',
        'security.protocol': 'SASL_SSL',
        'sasl.mechanism': 'PLAIN',
        'sasl.username': 'D7SZNCMCQFSPSJH4',
        'sasl.password': 'yMP5kRqdsoJutkGojOEuZXKb5iObdjDn6OkEtooyuIoJ1JfrRa+2H4j3Vl/m1t5u',
        'client.id': socket.gethostname()}

topic = "chatter"
producer = Producer(kafka_conf)

# FLASK CONF
app = Flask(__name__)
CORS(app)
UPLOAD_FOLDER = './new_docs'
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

# LLM CONF
model = os.environ.get("MODEL", "mistral")
embeddings_model_name = os.environ.get("EMBEDDINGS_MODEL_NAME", "all-MiniLM-L6-v2")
persist_directory = os.environ.get("PERSIST_DIRECTORY", "db")
target_source_chunks = int(os.environ.get('TARGET_SOURCE_CHUNKS', 4))
embeddings = HuggingFaceEmbeddings(model_name=embeddings_model_name)
db = Chroma(persist_directory=persist_directory, embedding_function=embeddings)
retriever = db.as_retriever(search_kwargs={"k": target_source_chunks})


# Langchain producing Kafka messages

class CustomHandler(BaseCallbackHandler):
    def on_llm_new_token(self, token: str, **kwargs) -> None:
        now = datetime.now().strftime("%d/%m/%Y %H:%M:%S")
        print(f"Producing message: {now} , {token}")
        producer.produce(topic, key=now, value=token)

llm = Ollama(model=model, callbacks=[CustomHandler()])
qa = RetrievalQA.from_chain_type(llm=llm, chain_type="stuff", retriever=retriever)


# API

@app.route('/mychat', methods=['POST'])
async def get_chat_data():
    print("Query:")
    data = request.get_json()
    query = data['query']
    print(query)

    # t = threading.Thread(target=ask)
    # t.start()

    fullTMPResp = qa(query)
    print(fullTMPResp)

    return jsonify(fullTMPResp)

@app.route('/upload', methods=['POST'])
def upload_file():
    # check if the post request has the file part
    if 'file' not in request.files:
        return 'No file part in the request.', 400

    file = request.files['file']

    # if user does not select file, file variable can be empty
    if file.filename == '':
        return 'No selected file.', 400

    if file and allowed_file(file.filename):
        filename = file.filename
        file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))

        res = subprocess.run(['python3', 'upload.py'], capture_output=True, text=True)
        print(res.stdout)

        response = {'result': 'ok'}
        return jsonify(response)


def allowed_file(filename):
    ALLOWED_EXTENSIONS = {'txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'}
    return '.' in filename and \
        filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS



if __name__ == '__main__':
    app.run(host="0.0.0.0",debug=True)
